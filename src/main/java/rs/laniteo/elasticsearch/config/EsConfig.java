package rs.laniteo.elasticsearch.config;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@Configuration
//@EnableElasticsearchRepositories(basePackages = "com.mkyong.book.repository")
public class EsConfig {

	@Value("${elasticsearch.host}")
    private String EsHost;

    @Value("${elasticsearch.port}")
    private int EsPort;

    @Value("${elasticsearch.clustername}")
    private String EsClusterName;

/*    @Bean
    public Client client() throws Exception {
    	Settings settings = Settings.settingsBuilder()
    	        .put("cluster.name", "myClusterName").build();
    	Client client = TransportClient.builder().settings(settings).build();
}*/
    }
