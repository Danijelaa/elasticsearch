package rs.laniteo.elasticsearch;

import javax.annotation.PostConstruct;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import rs.laniteo.elasticsearch.model.Student;
import rs.laniteo.elasticsearch.repository.StudentRepository;

@Component
public class TestData {

	@Autowired
	StudentRepository sr;
	@PostConstruct
	public void init(){
		System.out.println("Jesi, it's me.");
		
		/*Settings settings = Settings.settingsBuilder()
    	        .put("cluster.name", "my-application").build();
    	Client client = TransportClient.builder().settings(settings).build();*/
		Settings settings = Settings.settingsBuilder()
		        .loadFromSource("./src/main/resources/application.properties").build();
		Client client = TransportClient.builder().settings(settings).build();
		Student s1=new Student();
		s1.setId("1");
		s1.setName("Danijela");
		sr.save(s1);
		
		Student s2=new Student();
		s2.setId("2");
		s2.setName("Daca");
		sr.save(s2);
	}
}
