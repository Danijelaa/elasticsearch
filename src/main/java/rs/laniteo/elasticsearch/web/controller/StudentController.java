package rs.laniteo.elasticsearch.web.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import rs.laniteo.elasticsearch.model.Student;
import rs.laniteo.elasticsearch.repository.StudentRepository;

@RestController
@RequestMapping(value="/students")
public class StudentController {

	@Autowired
	StudentRepository sr;
	
	@RequestMapping(method=RequestMethod.GET)
	ResponseEntity<List<Student>> getAll(){
		List<Student> students=new ArrayList<Student>();
		Iterator<Student> it=sr.findAll().iterator();
		while(it.hasNext()){
			students.add(it.next());
		}
		return new ResponseEntity<List<Student>>(students, HttpStatus.OK);
	}
}
