package rs.laniteo.elasticsearch.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import rs.laniteo.elasticsearch.model.Student;

@Repository
public interface StudentRepository extends ElasticsearchRepository<Student, String>{

	 public Iterable<Student> findAll();
}
